<?php // https://www.codeofaninja.com/2015/08/display-twitter-feed-on-website.html

$all_tweets = array();

$feeds = array(
    'NASA',
    // 'MarsCuriosity',
    'spacex',
    'esa',
    'neiltyson',
    // 'NightValeRadio',
    'BBCBreaking',
    // 'TheOnion',
);

foreach ($feeds as $twitter_screenname) {
    $oauth_access_token = "2420011-JNcyNbEz7QQCesDLv7g1sYbNvcCdIkbFhMHd6dtNi7";
    $oauth_access_token_secret = "sKuXxgaRAFuJ5LZSCN86GQ4ldVNSfeiXBZ48lzLeV5u4M";
    $consumer_key = "WSZVw3L0nUDeJ98bNQInlHo4P";
    $consumer_secret = "0N40GRtmWZWx1WBIA8WyDVVss6WqZbJOAWUmaK6iF90L5oSBwV";

    $oauth = array(
        'oauth_consumer_key' => $consumer_key,
        'oauth_nonce' => time(),
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_token' => $oauth_access_token,
        'oauth_timestamp' => time(),
        'oauth_version' => '1.0'
    );
    $twitter_timeline = "user_timeline";
    $request = array(
        'count' => '2',
        'screen_name' => $twitter_screenname
    );
    $oauth = array_merge($oauth, $request);
    $baseURI = "https://api.twitter.com/1.1/statuses/$twitter_timeline.json";
    $method = "GET";
    $params = $oauth;

    $r = array();
    ksort($params);
    foreach($params as $key=>$value){
        $r[] = "$key=" . rawurlencode($value);
    }
    $base_info = $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
    $oauth['oauth_signature'] = $oauth_signature;
    $r = 'Authorization: OAuth ';
    $values = array();
    foreach($oauth as $key=>$value){
        $values[] = "$key=\"" . rawurlencode($value) . "\"";
    }
    $r .= implode(', ', $values);
    $header = array($r, 'Expect:');
    $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_HEADER => false,
        CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?". http_build_query($request),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false
    );
    $feed = curl_init();
    curl_setopt_array($feed, $options);
    $json = curl_exec($feed);
    curl_close($feed);

    // decode json format tweets
    $tweets = json_decode($json, true);

    foreach ($tweets as $tweet) {
        $all_tweets[] = $tweet;
    }
}

$json = $all_tweets;
