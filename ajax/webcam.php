<?php
$post = json_decode(file_get_contents("php://input"));
date_default_timezone_set('America/Denver');

class cameraData {

	public function get_latest_image() {
        $return = array();

        // Database variables!
        $mysql_host  = "pollard.house";
        $mysql_user  = "pollardhouse";
        $mysql_pass  = "use fact on government";
        $mysql_db    = "pollardhouse_data";
        $mysql_tbl   = "camera";

        $env_data_query = "SELECT * FROM " . $mysql_tbl . " ORDER BY id DESC LIMIT 1";
        $env_data = array();

        // Gather data from mysql database
        $mysql_conn = mysql_connect($mysql_host, $mysql_user, $mysql_pass) or die("Unable to connect to MySQL");
        $selected = mysql_select_db($mysql_db, $mysql_conn) or die("Could not select examples");
        $result = mysql_query($env_data_query);

        while ($row = mysql_fetch_array($result)) {
            // var_dump($row);
            $return["filename"] = $row["filename"];
            $return["timestamp"] = $row["timestamp"];
        }

        return $return;
    }

}

if (isset($post->action)) {
    switch ($post->action) {
        case 'get_latest_image':
            $cameraData = new cameraData;
            $image = $cameraData->get_latest_image();

            $return["url"] = "http://pollard.house/webcam_upload/".$image["filename"];
            $return["timestamp"] = $image["timestamp"];
            break;

        default:
            $return = array("message" => "Undefined action: " . $post->action);
            break;
    }
    echo json_encode($return);
} else {
    $error = array("message" => "No action specified");
    echo json_encode($error);
}
