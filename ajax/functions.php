<?php
date_default_timezone_set('America/Denver');

class weatherData {

	public function get_current_conditions() {

		$current_hour = date('G');
		$condition_code = "wi-";

		if ($current_hour >= 7 && $current_hour <= 20) {
			$condition_time = "day-";
			$current_condition = "sunny";
			$is_day = true;
		} else {
			$condition_time = "night-";
			$current_condition = "clear";
			$is_day = false;
		}

		if ($current_humidity >= 75) {
			$current_condition = "rain";
		} elseif ($current_humidity >= 50) {
			$current_condition = "cloudy";
		}

		$condition_code = $condition_code . $condition_time . $current_condition;


		// Database variables!
		$mysql_host  = "pollard.house";
		$mysql_user  = "pollardhouse";
		$mysql_pass  = "use fact on government";
		$mysql_db    = "pollardhouse_data";
		$mysql_tbl   = "environment";

		$env_data_query = "SELECT * FROM " . $mysql_tbl . " ORDER BY entry_id DESC LIMIT 1";
		$env_data = array();

		// Gather data from mysql database
		$mysql_conn = mysql_connect($mysql_host, $mysql_user, $mysql_pass) or die("Unable to connect to MySQL");
		$selected = mysql_select_db($mysql_db, $mysql_conn) or die("Could not select examples");
		$result = mysql_query($env_data_query);

		while ($row = mysql_fetch_array($result)) {
			$current_temp = intval($row["temp_f"]);
			$current_humidity = intval($row["humidity"]);
			$updated_date = $row["date"];
		}
		//
		// $current_temp = rand(0, 110);
		// $current_humidity = rand(1, 99);


		$return = array(
			'temp'		=> $current_temp,
			'humidity'	=> $current_humidity,
			'icon'		=> $condition_code,
			'updated'	=> $updated_date,
			);

		return $return;

	}


	/**
	 * Get weather data from database to populate chart.
	 * @param  string
	 * @return array
	 */
	public function get_weather($filter = null) {

		// if (!isset($filter) || $filter == "")
		$filter = '-1 day';

		// Database variables!
		$mysql_host  = "pollard.house";
		$mysql_user  = "pollardhouse";
		$mysql_pass  = "use fact on government";
		$mysql_db    = "pollardhouse_data";
		$mysql_tbl   = "environment";

		$query_date_limit = strtotime($filter);

		$env_data_query = "SELECT * FROM " . $mysql_tbl . " WHERE timestamp > ".$query_date_limit." ORDER BY entry_id DESC";
		$env_data = array();

		// Gather data from mysql database
		$mysql_conn = mysql_connect($mysql_host, $mysql_user, $mysql_pass) or die("Unable to connect to MySQL");
		$selected = mysql_select_db($mysql_db, $mysql_conn) or die("Could not select examples");
		$result = mysql_query($env_data_query);


		$current_index = 0;
		$max_counter = 4;
		$counter = $max_counter - 1;
		while ($row = mysql_fetch_array($result)) {
			$counter++;

			if ($counter == $max_counter) {
				$env_data[$row['entry_id']] = array(
					// "date"          => date('m/d/Y h:i:sa', $row["timestamp"]),
					"timestamp"     => $row["timestamp"],
					"temperature"   => $row["temp_f"],
					"humidity"      => $row["humidity"],
				);
			}
			if ($counter >= $max_counter) {
				$counter = 0;
			}

			$current_index++;
		}


		mysql_close($mysql_conn);
		$env_data = array_reverse($env_data); // Flip the array on it's head

		// Massage the data
		$temp_data = $humidity_data = array();
		foreach ($env_data as $id => $data) {
			$temp_data_datetime[] = array((($data['timestamp']+date('Z'))*1000), number_format($data['temperature'],1));
			$humidity_data_datetime[] = array((($data['timestamp']+date('Z'))*1000), number_format($data['humidity'],1));
		}

		$return['temp_data'] = $temp_data_datetime;
		$return['humidity_data'] = $humidity_data_datetime;

		return $return;
	}
}
