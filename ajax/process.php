<?php
require_once("functions.php");
$return = array();

$weatherData = new weatherData;
$weatherArray = $weatherData->get_current_conditions();

$return['temp'] = $weatherArray['temp'];
$return['icon'] = $weatherArray['icon'];
$return['humidity'] = $weatherArray['humidity'];
$return['updated'] = $weatherArray['updated'];
$return['success'] = "Temp: " . $weatherArray['temp'] . "f";

echo json_encode($return);
