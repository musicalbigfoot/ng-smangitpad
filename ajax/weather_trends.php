<?php
require_once("functions.php");
$return = array();

$weatherData = new weatherData;
$weatherArray = $weatherData->get_weather();

$return["humidity_data"] = str_replace("\"", "", json_encode($weatherArray['humidity_data'], true));
$return["temp_data"] = str_replace("\"", "", json_encode($weatherArray['temp_data'], true));

echo json_encode($return);
