var smangitPadApp = angular.module('smangitPadApp', [
	'ngRoute',
	'ngAnimate',
	'ngSanitize',
	'ngToast',
	'firebase',
	'googleApi',
	'angularModalService',
	'smangitPadApp.auth',
	'smangitPadApp.register'
])
.filter('linkUsername', function() {
	return function(text) {
		return '<a href="http://twitter.com/' + text.slice(1) + '">' + text + '</a>';
	};
})
.filter('linkHashtag', function() {
	return function(text) {
		return '<a href="http://twitter.com/search/%23' + text.slice(1) + '">' + text + '</a>';
	};
})
.filter('tweet', function() {
	return function(text) {
		var urlRegex = /((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/g;
		var twitterUserRegex = /@([a-zA-Z0-9_]{1,20})/g;
		var twitterHashTagRegex = /\B#(\w+)/g;

		text = text.replace(urlRegex," <a href='$&' target='_blank'>$&</a>").trim();
		text = text.replace(twitterUserRegex,"<a href='http://www.twitter.com/$1' target='_blank'>@$1</a>");
		text = text.replace(twitterHashTagRegex,"<a href='http://twitter.com/search/%23$1' target='_blank'>#$1</a>");

		return text;
	};
});

// Configure the application
smangitPadApp.config(['$routeProvider', '$locationProvider', '$httpProvider', 'googleLoginProvider', function ($routeProvider, $locationProvider, $httpProvider, googleLoginProvider) {

	$routeProvider
		.when('/', {
			templateUrl : 'views/dashboard/index.html',
			controller : 'DashboardCtrl'
			// resolve: {
			// 	"currentAuth": ["Auth", function(Auth) {
			// 		return Auth.$requireAuth();
			// 	}]
			// }
		});

	// sec: tQG3JMEI1E_OqI75mz65ljfx
	googleLoginProvider.configure({
        clientId: '76010610476-ntujt1086g0o1vluj35nc4queelig1uo.apps.googleusercontent.com',
		clientSecret: "tQG3JMEI1E_OqI75mz65ljfx",
        scopes: ["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/calendar", "https://www.googleapis.com/auth/plus.login"]
    });

	$locationProvider.html5Mode(true);
	$httpProvider.defaults.withCredentials = true;
}]);


// Declare the main Firebase App URL
smangitPadApp.constant('FirebaseUrl', 'https://smangitpad.firebaseio.com/');


// Main Application controller
smangitPadApp.controller('MainCtrl', ['$scope', '$location', '$timeout', 'Auth', 'FirebaseUrl', '$firebaseObject', 'ngToast',
	function($scope, $location, $timeout, Auth, FirebaseUrl, $firebaseObject, ngToast) {

	console.log("Welcome to the SmangitPad Dashboard!");

	$scope.authStatus = false;
	// Update the main layout elements on auth change.
	Auth.$onAuth(function(authData){
		// console.log("Auth Status Change...");
		$scope.authStatus = (authData) ? true : false;
		// $scope.views = LayoutViews.updateLayout($scope.authStatus);
	});
}]);
