var smangitPadAppAuth = angular.module('smangitPadApp.auth', [
	'ngRoute',
	'firebase'
]);

// Declared route
smangitPadAppAuth.config(['$routeProvider', function($routeProvider) {
    $routeProvider
		.when('/login', {
			templateUrl: 'views/auth/login.html',
			controller: 'LoginCtrl'
		})
		.when('/logout', {
			templateUrl: 'views/auth/login.html',
			controller: 'LogoutCtrl'
		});
}]);


smangitPadAppAuth.controller('LoginCtrl', ['$scope', '$timeout', '$location', '$firebaseAuth', 'Auth', function($scope, $timeout, $location, $firebaseAuth, Auth) {
	var firebaseObj = new Firebase("https://smangitpad.firebaseio.com");
	var loginObj = $firebaseAuth(firebaseObj);

	// Declare default values
	$scope.login = {
		username: "",
		password: ""
	};
	$scope.login_message = "";

	$scope.Login = function(e) {
		e.preventDefault();
		$scope.login_message = "Loading...";
		var username = $scope.login.email;
		var password = $scope.login.password;
		if (username !== "" && password !== "") {
			loginObj.$authWithPassword({
				email: username,
				password: password
			})
			.then(function(user) {
				console.log('Authentication successful');
				var authStatus = {uid:user.uid};
				$location.path('/');
			}, function(error) {
				console.log('Authentication failure');
				$scope.login_message = "Authentication Failed";
			});
		} else {
			$scope.login_message = "Enter Username and Password";
			$timeout(function(){
				$scope.login_message = "";
			}, 2000);
		}
	}
}]);


smangitPadApp.controller('LogoutCtrl', ['$scope', '$timeout', '$location', '$firebaseAuth', function($scope, $timeout, $location, $firebaseAuth){
	var firebaseObj = new Firebase("https://smangitpad.firebaseio.com");
	var logoutObj = $firebaseAuth(firebaseObj);

	logoutObj.$unauth();
	$timeout(function(){
		$location.path('/login');
	}, 10);
}]);


smangitPadApp.factory("Auth", ["$firebaseAuth", function($firebaseAuth) {
	var ref = new Firebase("https://smangitpad.firebaseio.com");
	return $firebaseAuth(ref);
}]);


smangitPadApp.run(["$rootScope", "$location", function($rootScope, $location) {
	$rootScope.$on("$routeChangeError", function(event, next, previous, error) {
		console.log(event);
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		if (error === "AUTH_REQUIRED") {
			$location.path("/login");
		}
	});
}]);
