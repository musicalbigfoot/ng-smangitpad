var smangitPadAppRegister = angular.module('smangitPadApp.register', [
	'ngRoute',
	'firebase'
]);

// Declared route
smangitPadAppRegister.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/register', {
			templateUrl: 'views/auth/register.html',
			controller: 'RegisterCtrl'
		});
}]);

smangitPadAppRegister.controller('RegisterCtrl', ['$scope', '$timeout', '$location', '$firebaseAuth', '$firebaseArray', '$firebaseObject', 'FirebaseUrl', function($scope, $timeout, $location, $firebaseAuth, $firebaseArray, $firebaseObject, FirebaseUrl) {
	var firebaseObj = new Firebase("https://smangitpad.firebaseio.com");
	var auth = $firebaseAuth(firebaseObj);

	$scope.register = {
		"email": "",
		"password": "",
		"username": ""
	};
	$scope.user = {
		email: "",
		password: ""
	};
	$scope.register_message = "";

	$scope.registerAccount = function() {

		if (!$scope.registerForm.$invalid && $scope.register.email !== "" && $scope.register.password !== "") {

			var email = $scope.register.email;
			var password = $scope.register.password;
			var username = $scope.register.username;

			if (email && password) {
				auth.$createUser({email, password})
				.then(function(userData) {

					// Log the new user into the system
					return auth.$authWithPassword({
						email: email,
						password: password
					});

				}).then(function(authData) {
					var user_id = authData.uid;

					var usersRef = new Firebase(FirebaseUrl + 'users');
					var newUser = {
						"email": email,
						"username": username
					};

					console.log("User Created");
					usersRef.child(user_id).set(newUser, function(){
						console.log("Profile Updated");
						$timeout(function(){
							$location.path('/');
						}, 100);
					});

				}).catch(function(error) {
					$scope.regError = true;
					$scope.register_message = error.message;
					console.log(error);
				});
			}

		} else {
			console.log('Invalid form submission');
		}

	}

}]);
