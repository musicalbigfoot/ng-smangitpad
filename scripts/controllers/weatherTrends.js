smangitPadApp.directive('weatherTrendsChart', ['WeatherTrends', function(WeatherTrends){
    return {
        restrict: 'C',
        // replace: true,
        scope: {
            items: '=',
            temp: '='
        },
        controller: function($scope, $element, $attrs, WeatherTrends){
            // console.log('Chart Init');
            // $scope.temp_data = WeatherTrends.temp_data;
            // console.log($scope.temp_data);
        },
        // template: '<div id="chartContainer" style="margin: 0 auto">error</div>',
        link: function (scope, element, attrs) {
            var chart = new Highcharts.Chart({
                title: {
                    enabled: false,
                    text: ''
                },
                chart: {
                    renderTo: $(".weather-trends-chart")[0],
                    backgroundColor: "transparent",
                },
                xAxis: {
                    labels: {
                        enabled: false
                    },
                    tickPixelInterval: 50,
                    tickColor: "#0A2342",
                    lineColor: "#0A2342",
                    gridLineWidth: 1,
                    gridLineColor: "#0A2342",
                    type: 'datetime'
                },
                yAxis: [{
                    title: {
                        enabled: false,
                    },
                    gridLineWidth: 1,
                    gridLineColor: "#0A2342",

                    labels: {
                        format: '{value}°F',
                        style: {
                            color: "#1657a2",
                            fontWeight: "bold"
                        }
                    },
                    opposite: true,
                    // tickPositions: [20, 30, 40, 50, 60, 70, 80, 90]
                }],
                tooltip: {
                    borderRadius: 0,
                    borderWidth: 0,
                    crosshairs: [false, false]
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series: [{
                    yAxis: 0,
                    lineWidth: 6,
                    turboThreshold: 10000,
                    color: {
                        linearGradient: [0, 0, 0, 500],
                        // linearGradient: { x1: 0, y1: 0, x2: 0, y2: 2 },
                        stops: [
                            [0.0, '#e2431e'],
                            [0.1, '#ef851c'],
                            [0.2, '#f4b400'],
                            [0.3, '#c3d03f'],
                            [0.4, '#63a74a'],
                            [0.5, '#1a8763'],
                            [0.6, '#1c91c0'],
                            [0.7, '#43459d'],
                            [0.85, '#703593'],
                            [1, '#99003e']
                        ]
                    },
                    marker: {
                        enabled: false,
                    },
                    tooltip: {
                        valueSuffix: '°F',
                    },
                    name: 'Temperature',
                    animation: {
                        duration: 500
                    },
                    type: "spline",
                    data: scope.temp
                }]
            });

            scope.$watch("temp", function (newValue) {
                if (newValue) {
                    var newData = JSON.parse(newValue);
                    chart.series[0].setData(newData, true);
                }
            }, true);
        }
    }
}]);





smangitPadApp.factory('WeatherTrends', ['$rootScope', '$interval', '$http', function($rootScope, $interval, $http){

	var WeatherTrends = {
        temp_data: [],
        humidity_data: [],
        initWeatherTrends: function(){
            WeatherTrends.updateWeatherTrends();
            var trends_interval = $interval(function(){
                WeatherTrends.updateWeatherTrends();
            }, 300000);
		},
		updateWeatherTrends: function(){
            $rootScope.toggleLoading('weather-trends', 'on');
			$http({
				method  : 'POST',
				url     : '/ajax/weather_trends.php',
				data    : { "action": "get_weather_trends" },
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			.success(function(data) {
                // console.log("Weather Updated");
                $rootScope.temp_data = data.temp_data;
                $rootScope.humidity_data = data.humidity_data;
                $rootScope.toggleLoading('weather-trends');
			});
		}
	};

	return WeatherTrends;
}]);
