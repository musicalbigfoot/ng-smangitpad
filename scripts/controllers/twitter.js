smangitPadApp.factory('Twitter', ['$rootScope', '$interval', '$http', function($rootScope, $interval, $http){

	var Twitter = {
        numSlides: 0,
        initFeed: function(){
            $rootScope.twitterFeed = JSON.parse(localStorage.getItem('twitterFeed'));
            Twitter.updateFeed();
			$interval(function(){
				Twitter.updateFeed();
			}, 900 * 1000); // 900 = 5min

			$interval(function(){
				Twitter.nextTweet();
			}, 18 * 1000);
		},
		updateFeed: function(){
			$rootScope.toggleLoading('twitter', 'on');
			$http({
				method  : 'POST',
				url     : '/ajax/twitter.php',
				data    : { "action": "get_latest_feed" },
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			.success(function(data){
                // console.log("Twitter Feed Updated");
				// console.log(data);
                localStorage.setItem('twitterFeed', JSON.stringify(data));
                $rootScope.twitterFeed = data;
				$rootScope.toggleLoading('twitter');
			})
			.error(function(data){
                console.error(data);
				$rootScope.toggleLoading('twitter');
			});
		},

		nextTweet: function(){
			var slideSpeed = 2000;
			var $current = $('[data-card-type="twitter"] .current');
			if ($current.next('li').is(':last-child')) {
				var $next = $('[data-card-type="twitter"] ul li:first-child');
			} else {
				var $next = $current.next('li');
			}

			$next.slideDown(slideSpeed);
			$current.slideUp(slideSpeed, function(){
				$(this).removeClass('current');
				$next.addClass('current');
			});
		}
	};

	return Twitter;
}]);
