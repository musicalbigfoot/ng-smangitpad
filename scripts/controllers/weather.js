smangitPadApp.factory('Weather', ['$rootScope', '$interval', '$http', function($rootScope, $interval, $http){

	var Weather = {
		initWeather: function(){
			Weather.updateCurrentConditions();
			var weather_interval = $interval(function(){
				Weather.updateCurrentConditions();
			}, 60000);
		},
		updateCurrentConditions: function(){
			// console.log("Updating Current Weather Conditions");
			$rootScope.toggleLoading('weather-current', 'on');
			$http({
				method  : 'POST',
				url     : '/ajax/process.php',
				data    : { "action": "get_current_conditions" },
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			.success(function(data) {
				$rootScope.condition_code = data.icon;
				$rootScope.current_temp = data.temp;
				$rootScope.current_humidity = data.humidity;
				$rootScope.weather_updated = moment(data.updated).format('MMM D - h:mma');
				$rootScope.toggleLoading('weather-current');
			});
		}
	};

	return Weather;
}]);
