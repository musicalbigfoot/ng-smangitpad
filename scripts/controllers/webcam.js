smangitPadApp.factory('Webcam', ['$rootScope', '$interval', '$http', function($rootScope, $interval, $http){

	var Webcam = {
		initWebcam: function(){
			Webcam.updateLatestImage();
			$interval(function(){
				Webcam.updateLatestImage();
			}, 90000);
		},
		updateLatestImage: function(){
			$rootScope.toggleLoading('webcam', 'on');
			$http({
				method  : 'POST',
				url     : '/ajax/webcam.php',
				data    : { "action": "get_latest_image" },
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			.success(function(data){
				$rootScope.webcam = {
					"url": data.url,
					"timestamp": moment(data.timestamp*1000).format('MMM D - h:mma')
				};
				$rootScope.toggleLoading('webcam');
			})
			.error(function(data){
				$rootScope.toggleLoading('webcam');
			});
		}
	};

	return Webcam;
}]);
