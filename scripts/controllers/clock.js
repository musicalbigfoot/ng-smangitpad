smangitPadApp.factory('Clock', ['$rootScope', '$interval', function($rootScope, $interval){

	var Clock = {
		initClock: function(){
			var time_format = "h:mm",
				date_format = "MMM D, YYYY";
			$rootScope.clock = moment().format(time_format);
			$rootScope.clock_date = moment().format(date_format);
			var clock_interval = $interval(function(){
				$rootScope.clock = moment().format(time_format);
				$rootScope.clock_date = moment().format(date_format);
			}, 5000);
		}
	};

	return Clock;
}]);
