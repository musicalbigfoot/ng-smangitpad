smangitPadApp.factory('Calendar', ['$rootScope', '$interval', 'googleLogin', 'googlePlus', 'googleCalendar', function($rootScope, $interval, googleLogin, googlePlus, googleCalendar){

	if (localStorage.getItem('eventRows')) {
		if (JSON.parse(localStorage.getItem('eventRows')).length > 0) {
			$rootScope.eventRows = JSON.parse(localStorage.getItem('eventRows'));
		}
	}

	var Calendar = {
		events: [],
		initCalendar: function(){
			Calendar.updateCalendar();

			$interval(function(){
				Calendar.updateCalendar();
			}, 600000);
		},
		resetCalendar: function(){
			Calendar.events = [];
			$rootScope.eventRows = [];
		},
		updateCalendar: function(){
			// console.log("Updating Calendar");
			$rootScope.toggleLoading('calendar', 'on');

			var timeMin = new Date();
			var timeMax = new Date();
			timeMin.setHours(0,0,0,0);
			timeMax.setHours(167,59,59,0); // Add 7 days from now

			// this.calendarItems = googleCalendar.listEvents({calendarId: this.selectedCalendar.id});
			var events = googleCalendar.listEvents({
				"calendarId": "5m0ampqiabtl6be8btsd5ottsg@group.calendar.google.com",
				"timeMin": timeMin.toISOString(),
				"timeMax": timeMax.toISOString(),
				"singleEvents": true,
				"maxResults": 99,
				"orderBy": "startTime"
			}).then(function(data){
				for (event of data) {

					var isAllDay = false,
						isHappening = false,
						isPast = false,
						newEvent = {};

					// When does this event start?
					var whenStart = event.start.dateTime;
					if (!whenStart) {
						whenStart = event.start.date;
						isAllDay = true;
					}
					var startHour = moment(whenStart).format('h'),
						startMinute = moment(whenStart).format('mm'),
						startDisplay = startHour;
					if (parseInt(startMinute) > 0) {
						startDisplay = startHour+':'+startMinute;
					}

					// When does this event end?
					var whenEnd = event.end.dateTime;
					if (!whenEnd) {
						whenEnd = event.end.date;
						isAllDay = true;
					}
					var endHour = moment(whenEnd).format('h'),
						endMinute = moment(whenEnd).format('mm'),
						endDisplay = endHour;
					if (parseInt(endMinute) > 0) {
						endDisplay = endHour+':'+endMinute;
					}

					// Is this event still happening?
					if ( moment(whenStart) <= moment() && moment(whenEnd) >= moment() && !isAllDay) {
						isHappening = true;
					}

					// Is this event completely over?
					if ( moment(whenStart) <= moment() && moment(whenEnd) <= moment() && !isAllDay) {
						isPast = true;
					}


					// Loop through all attendees, create a string with is
					var eventAttendees = "";
					if (event.attendees && event.attendees.length > 0) {
						for (var y = 0; y < event.attendees.length; y++) {
							var thisAttendee = event.attendees[y];
							if (thisAttendee.displayName) {
								// var thisName = thisAttendee.displayName.split(' ');
								// eventAttendees += thisName[0] + ', ';
							}
						}
					}

					var timeDisplay = (isAllDay) ? "All Day" : startDisplay+'-'+endDisplay;

					// console.log(event);

					newEvent.title = event.summary;
					newEvent.desc = event.description;

					if (event.creator.displayName) {
						newEvent.creator = event.creator.displayName.split(' ')[0];
					}
					newEvent.attendees = eventAttendees.replace(/, $/,'');
					newEvent.colorId = event.colorId;

					newEvent.startDate = whenStart;
					newEvent.endDate = whenEnd;
					newEvent.timeDisplay = timeDisplay;

					newEvent.isAllDay = isAllDay;
					newEvent.isPast = isPast;
					newEvent.isHappening = isHappening;

					Calendar.events.push(newEvent);
				}

				var i = 0;
				var eventRows = [];
				while (i < 7) {

					var todaysEvents = {
						"day": moment().add(i, 'd').format('ddd'),
						"date": moment().add(i, 'd').format('DD'),
						"isWeekend": (moment().add(i, 'd').format('E') >= 6) ? true : false,
						"events": []
					};

					for (event of Calendar.events) {
						if (moment().add(i,'d').startOf('day').isSame(moment(event.startDate).startOf('day'))) {
							todaysEvents.events.push(event);
						}
					}
					eventRows.push(todaysEvents);

					i++;
				}

				Calendar.resetCalendar();
				$rootScope.eventRows = eventRows;
				localStorage.setItem("eventRows", JSON.stringify(eventRows));

				$rootScope.toggleLoading('calendar');
			});
		}
	};

	return Calendar;
}]);
