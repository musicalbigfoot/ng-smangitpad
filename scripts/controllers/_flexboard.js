// Declared route
smangitPadApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
		.when('/flexboard', {
			templateUrl: 'views/flexboard/index.html',
			controller: 'FlexboardCtrl'
		});
}]);

smangitPadApp.controller('FlexboardCtrl', ['$scope', '$rootScope', '$http', '$location', '$timeout',
	function($scope, $rootScope, $http, $location, $timeout) {

    console.log("Flexboard Initiated");

    $scope.flexboardRows = [
        {
            cards: [{
                id: "card1"
            },
            {
                id: "card2"
            }]
        },
        {
            cards: [{
                id: "card1"
            }]
        },
        {
            cards: [{
                id: "card1"
            },
            {
                id: "card2"
            },
            {
                id: "card3"
            }]
        }
    ];
    console.log($scope.flexboardRows);

}]);
