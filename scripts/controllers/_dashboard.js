smangitPadApp.controller('DashboardCtrl', ['$scope', '$rootScope', '$http', '$location', '$timeout', '$window', 'Clock', 'Webcam', 'Weather', 'WeatherTrends', 'Twitter', 'Calendar', 'Auth', 'FirebaseUrl', '$firebaseObject', 'ngToast', 'googleLogin', 'googleCalendar', 'googlePlus',
	function($scope, $rootScope, $http, $location, $timeout, $window, Clock, Webcam, Weather, WeatherTrends, Twitter, Calendar, Auth, FirebaseUrl, $firebaseObject, ngToast, googleLogin, googleCalendar, googlePlus) {

	var current_hour = 20;
	var condition_code = "wi-";
	var condition_time, current_condition, is_day;
	var current_humidity = 24;

	if (current_hour >= 7 && current_hour <= 20) {
		condition_time = "day-";
		current_condition = "sunny";
		is_day = true;
	} else {
		condition_time = "night-";
		current_condition = "clear";
		is_day = false;
	}

	if (current_humidity >= 75) {
		current_condition = "rain";
	} else if (current_humidity >= 50) {
		current_condition = "cloudy";
	}


	$rootScope.toggleLoading = function(cardType, status){
		if (cardType != "") {
			var $loading = $('smangit-card[data-card-type="'+cardType+'"] smangit-loading');
			if (status == "on") {
				$loading.attr('data-status','active');
			} else {
				setTimeout(function () {
					$loading.attr('data-status','');
				}, 1500);
			}
		}
	};


	Clock.initClock();
	Weather.initWeather();
	WeatherTrends.initWeatherTrends();
	Webcam.initWebcam();
	Twitter.initFeed();

	$timeout(function(){
		$window.location.reload();
	}, 3600*1000); // 6hr timeout, reload the entire page. keep it fresh, homie!


	if (!googleLogin.currentUser) {
		$timeout(function(){
			googleLogin.login();
		}, 2000);
	}

	$scope.googleLogin = function(){
		googleLogin.login();
	};

	$scope.$on("googlePlus:loaded", function() {
		googlePlus.getCurrentUser().then(function(user) {
			$scope.currentUser = user;
			Calendar.initCalendar();
		});
	});
	$scope.currentUser = googleLogin.currentUser;

}]);
