// https://www.kycosoftware.com/blog/simple-and-awesome-gulp-setup

// Load up all of our awesome libraries
var gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	gutil 			= require('gulp-util'),
	notify 			= require('gulp-notify'),
	plumber 		= require('gulp-plumber'),
	include 		= require('gulp-include'),
	uglify 			= require('gulp-uglifyjs'),
	sourcemaps		= require('gulp-sourcemaps'),
	autoprefixer 	= require('gulp-autoprefixer'),
	browserSync		= require('browser-sync').create();


// STYLES
gulp.task('styles', function() {
	gulp.src([
			// 'scss/**/*.scss'
			'scss/app.scss'
		])
		.pipe(sourcemaps.init())
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(sass({
			style: 'compressed'
		}).on('error', sass.logError))
		.pipe(autoprefixer())
		//.pipe(notify({
		//	message: "Get SASSy with it!",
		//	onLast: true
		//}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./css/'))
		.pipe(browserSync.stream());
});


// SCRIPTS
gulp.task('scripts', function() {
	gulp.src([
			// 'scripts/vendor/*.js',
			// 'scripts/vendor/jquery.min.js',
			// 'scripts/vendor/angular.min.js',
			// 'scripts/vendor/angular-route.min.js',

			'scripts/app.js'
		])
		.pipe(sourcemaps.init())
		.pipe(plumber({
			errorHandler: onError
		}))
		// .pipe(uglify('app.js', {
		// 	outSourceMap: true,
		// 	compress: false
		// }))
		//.pipe(notify({
		//	message: "JS Done",
		//	onLast: true
		//}))
		.on('error', console.log)
		.pipe(sourcemaps.write('.'))
		// .pipe(gulp.dest("js/"))
		.pipe(browserSync.stream());
});


// HTML
gulp.task('html', function() {
	gulp.src('*.html')
		// .pipe(notify({
		// 	message: "HTML Updated",
		// 	onLast: true
		// }))
		.pipe(browserSync.stream());
});


// Default 'gulp' task, start livereload and
// watch scripts/scss files for changes
gulp.task('default', function() {
	browserSync.init({
        proxy: "smangit.pad"
    });
	gulp.watch('**/*.html',['html']);
	gulp.watch('scss/**/*.scss',['styles']);
	gulp.watch('scripts/**/*.js',['scripts']);
});


// Error handling function
var onError = function (err) {
	gutil.log(gutil.colors.green(err));
};
